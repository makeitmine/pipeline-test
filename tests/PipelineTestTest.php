<?php
declare(strict_types=1);

require_once("lib/PipelineTest.php");

use PHPUnit\Framework\TestCase;
use PipelineTest\Testeroo; 

final class WhateverTest extends TestCase 
{
    public function testIsThingAString(): void 
    {
        $testClass = new Testeroo; 
        $this->assertTrue($testClass->isString('asdfasdfaf'));
    }

    public function testIsThingNotAString(): void 
    {
        $testClass = new Testeroo; 
        $this->assertFalse($testClass->isString(13435758));
    }
}